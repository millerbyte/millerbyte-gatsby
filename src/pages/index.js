import React from "react"
import { Link } from "gatsby"
import Header from "../components/Header"

// import Layout from "../components/layout"

const IndexPage = () => (
  <React.Fragment>
    <Header />
    <div class="grid grid-cols-1 md:grid-cols-3 gap-3 p-3">
      <div class="bg-yellow-200 rounded-lg shadow-md flex justify-center items-center text-6xl text-white cursor-pointer hover:opacity-50 hover:text-black">
        1
      </div>
      <div class="bg-red-200 rounded-lg shadow-md flex justify-center items-center text-6xl text-white cursor-pointer hover:opacity-50">
        2
      </div>
      <div class="bg-blue-200 rounded-lg shadow-md flex justify-center items-center text-6xl text-white cursor-pointer hover:opacity-50">
        3
      </div>
      <div class="bg-green-200 rounded-lg shadow-md flex justify-center items-center text-6xl text-white cursor-pointer hover:opacity-50">
        4
      </div>
    </div>
    <Link to="/page-2/">Test</Link>
  </React.Fragment>
)

export default IndexPage
