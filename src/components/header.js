import React from "react"

const Header = () => {
  return (
    <nav class="bg-gray-800 flex items-center justify-between p-3 flex-grow-0">
      <div class="flex justify-between text-gray-200 md:w-2/3 items-center">
        <div class="px-2 py-1">
          <div class="w-8 h-8 bg-purple-700 cursor-pointer hover:opacity-50 rounded-full"></div>
        </div>
        <div class="hover:bg-gray-500 cursor-pointer py-1 px-2 rounded-md hidden md:block bg-gray-900">
          Team
        </div>
        <div class="hover:bg-gray-500 cursor-pointer py-1 px-2 rounded-md hidden md:block">
          Projects
        </div>
        <div class="hover:bg-gray-500 cursor-pointer py-1 px-2 rounded-md hidden md:block">
          Calendar
        </div>
        <div class="hover:bg-gray-500 cursor-pointer py-1 px-2 rounded-md hidden md:block">
          Reports
        </div>
      </div>
      <div class="flex justify-between">
        <div class="p-2">
          <div class="w-4 h-4 bg-red-400 rounded-full cursor-pointer hover:opacity-50"></div>
        </div>
        <div class="p-2">
          <div class="w-4 h-4 bg-green-400 rounded-full cursor-pointer hover:opacity-50"></div>
        </div>
        <div class="p-2">
          <div class="w-4 h-4 bg-blue-400 rounded-full cursor-pointer hover:opacity-50"></div>
        </div>
      </div>
    </nav>
  )
}

export default Header
